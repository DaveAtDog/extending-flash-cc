// utility
function tr()
{
	fl.trace(Array.prototype.slice.call(arguments));
}


var dom = fl.getDocumentDOM();

dom.getTimeline().setSelectedFrames([]);

var frameRate = dom.frameRate;

var currentFrame = dom.getTimeline().currentFrame;

var framesToAdd = Math.round(frameRate/2);

dom.getTimeline().insertFrames(framesToAdd, true, currentFrame);

dom.getTimeline().setSelectedFrames([]);
