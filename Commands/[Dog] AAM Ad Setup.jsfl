/* 

AAM AD SETUP

Updates frame rate to 30fps.
Adds a correctly drawn header in it's own layer.
Addds a corretly drawn ketline on it's own layer.

*/

// new header sizes calc


function calcPerc(width)
{
	if (width <= 160)
	{
		return 0.25;
	}
	else if (width <= 300)
	{
		return 0.21;
	}
	else
	{
		return 0.12;
	}
}

// utility

function tr()
{
	fl.trace(Array.prototype.slice.call(arguments));
}

var dom = fl.getDocumentDOM();
dom.getTimeline().setSelectedFrames([]);

var lib = dom.library;

var stageH = dom.height;
var stageW = dom.width;

var titleHeader = 'Header';
var titleKeyline = 'Keyline';
var titleOverlay = 'Overlay';
var titleBackground = 'Background';
var titleBtnBackground = 'Btn Background';

var titleHeaderLeftFill = 'Header Left Fill';
var titleHeaderLeftMask = 'Header Left Mask';
var titleHeaderRightFill = 'Header Right Fill';
var titleHeaderRightMask = 'Header Right Mask';
var titleWhiteBox = 'Header White Box';
// ------------------------------------------------------------------------- //
// SET FRAME RATE
dom.frameRate = 30;
// ------------------------------------------------------------------------- //
//Background Fill
var myBackroundFill = dom.getCustomFill();
myBackroundFill.style = "solid";
myBackroundFill.color = 0xFFFFFF;
// ------------------------------------------------------------------------- //
//TEMP Fill
var myMaskFill = dom.getCustomFill();
myMaskFill.style = "solid";
myMaskFill.color = 0xFF00FF;
// ------------------------------------------------------------------------- //
// Primary fill & alternate colours
var myPrimaryFillBlue = dom.getCustomFill();
myPrimaryFillBlue.style = "solid";
myPrimaryFillBlue.color = 0x002664;

var myPrimaryFillPurple = dom.getCustomFill();
myPrimaryFillPurple.style = "solid";
myPrimaryFillPurple.color = 0x5D0749;

var myPrimaryFill;
// ------------------------------------------------------------------------- //
// Secondary Fill
var mySecondaryFill = dom.getCustomFill();
mySecondaryFill.style = "solid";
mySecondaryFill.color = 0x1A9CE3;
// ------------------------------------------------------------------------- //
// Stroke
// Fix from answer: http://stackoverflow.com/questions/4869979/jsfl-setcustomstroke-does-not-work-with-stroke-style-nostroke
dom.swapStrokeAndFill();
var tempFill = dom.getCustomFill("toolbar");
tempFill.style = "noFill";
dom.setCustomFill(tempFill);
dom.swapStrokeAndFill();

// vars
var headerHeight = 11;
var gap = 3;
var angleOffset = 6;
var bezelHorizontal = 8;
var bezelVertical = 6;
var brandBarPerc = calcPerc(stageW);

var headerRightWidth = Math.round((stageW - (bezelHorizontal * 2)) * brandBarPerc);

var theLeftPolygon = getLeftPolygon(stageW - (bezelHorizontal * 2));
var theSecondaryPolygonMask = getSecondaryPolygonMask(stageW - (bezelHorizontal * 2));
var theSecondaryPolygonFill = getSecondaryPolygonFill(stageW - (bezelHorizontal * 2));

function getLeftPolygon(width)
{
	var result = [
		[0, 0],
		[width - headerRightWidth - gap, 0],
		[width - headerRightWidth - gap - angleOffset, headerHeight],
		[0, headerHeight],
		[0, 0]
	];

	return result;
}

// ------------------------------------------------------------------------- //

function getSecondaryPolygonMask(width)
{
	var result = [
		[width - headerRightWidth, 0],
		[width, 0],
		[width, headerHeight],
		[width - headerRightWidth - angleOffset, headerHeight],
		[width - headerRightWidth, 0]
	];

	return result;
}

function getSecondaryPolygonFill(width)
{
	var result = [
		[width - headerRightWidth, 0],
		[width + angleOffset, 0],
		[width, headerHeight],
		[width - headerRightWidth - angleOffset, headerHeight],
		[width - headerRightWidth, 0]
	];

	return result;
}

// ------------------------------------------------------------------------- //

function polygonToPath(thePolygon)
{
	var path = fl.drawingLayer.newPath();
	var index = 0;
	while (index < thePolygon.length)
	{
		path.addPoint(thePolygon[index][0], thePolygon[index][1]);
		index++;
	}

	return path;
}

// ------------------------------------------------------------------------- //

function drawHeader()
{
	var curPath;
	var myPrimaryFill = (result.type == 'Blue') ? myPrimaryFillBlue : myPrimaryFillPurple;

	// ------------------------------------------------------------------------- //
	// BUILD SHAPES

	lib.addNewItem('movie clip', titleHeaderLeftFill);
	lib.editItem(titleHeaderLeftFill);

	curPath = polygonToPath(theLeftPolygon);
	dom.setCustomFill(myPrimaryFill);
	curPath.makeShape();

	fl.drawingLayer.endDraw();
	fl.getDocumentDOM().exitEditMode();

	// ------------------------------------------------------------------------- //

	lib.addNewItem('movie clip', titleHeaderLeftMask);
	lib.editItem(titleHeaderLeftMask);

	curPath = polygonToPath(theLeftPolygon);
	dom.setCustomFill(myMaskFill);
	curPath.makeShape();

	fl.drawingLayer.endDraw();
	fl.getDocumentDOM().exitEditMode();

	// ------------------------------------------------------------------------- //

	lib.addNewItem('movie clip', titleHeaderRightFill);
	lib.editItem(titleHeaderRightFill);

	curPath = polygonToPath(theSecondaryPolygonFill);
	dom.setCustomFill(mySecondaryFill);
	curPath.makeShape();

	fl.drawingLayer.endDraw();
	fl.getDocumentDOM().exitEditMode();

	// ------------------------------------------------------------------------- //

	lib.addNewItem('movie clip', titleHeaderRightMask);
	lib.editItem(titleHeaderRightMask);

	curPath = polygonToPath(theSecondaryPolygonMask);
	dom.setCustomFill(myMaskFill);
	curPath.makeShape();

	fl.drawingLayer.endDraw();
	fl.getDocumentDOM().exitEditMode();

	// ------------------------------------------------------------------------- //
	// white box
	lib.addNewItem('graphic', titleWhiteBox);
	lib.editItem(titleWhiteBox);

	dom.setCustomFill(myBackroundFill);
	dom.addNewRectangle(
	{
		left: -bezelHorizontal,
		top: -bezelVertical,
		right: stageW - bezelHorizontal,
		bottom: bezelVertical + headerHeight - bezelVertical
	}, 0);

	fl.drawingLayer.endDraw();
	fl.getDocumentDOM().exitEditMode();

	// ------------------------------------------------------------------------- //
	// HEADER

	lib.addNewItem('graphic', titleHeader);
	lib.editItem(titleHeader);


	fl.drawingLayer.beginDraw();

	// ------------------------------------------------------------------------- //

	var headerWhiteBox = lib.items[lib.findItemIndex(titleWhiteBox)];
	dom.getTimeline().addNewLayer(titleWhiteBox);

	dom.addItem(
	{
		x: 0,
		y: 0
	}, headerWhiteBox);

	dom.moveSelectionBy(
	{
		x: (stageW / 2) - bezelHorizontal,
		y: ((bezelVertical + headerHeight) / 2) - bezelVertical
	});

	// ------------------------------------------------------------------------- //

	var headerLeftFill = lib.items[lib.findItemIndex(titleHeaderLeftFill)];

	dom.getTimeline().addNewLayer(titleHeaderLeftFill);

	dom.addItem(
	{
		x: 0,
		y: 0
	}, headerLeftFill);

	dom.moveSelectionBy(
	{
		x: theLeftPolygon[1][0] / 2,
		y: theLeftPolygon[3][1] / 2
	});

	// ------------------------------------------------------------------------- //

	var headerLeftMask = lib.items[lib.findItemIndex(titleHeaderLeftMask)];

	dom.getTimeline().addNewLayer(titleHeaderLeftMask);

	dom.addItem(
	{
		x: 0,
		y: 0
	}, headerLeftMask);

	dom.moveSelectionBy(
	{
		x: theLeftPolygon[1][0] / 2,
		y: theLeftPolygon[3][1] / 2
	});



	// ------------------------------------------------------------------------- //

	var headerRightFill = lib.items[lib.findItemIndex(titleHeaderRightFill)];

	dom.getTimeline().addNewLayer(titleHeaderRightFill);

	dom.addItem(
	{
		x: 0,
		y: 0
	}, headerRightFill);

	dom.moveSelectionBy(
	{
		x: (theSecondaryPolygonMask[1][0] / 2) + ((theLeftPolygon[1][0] + gap) / 2),
		y: (theSecondaryPolygonMask[3][1] / 2)
	});

	// ------------------------------------------------------------------------- //

	var headerRightMask = lib.items[lib.findItemIndex(titleHeaderRightMask)];

	dom.getTimeline().addNewLayer(titleHeaderRightMask);

	dom.addItem(
	{
		x: 0,
		y: 0
	}, headerRightMask);

	dom.moveSelectionBy(
	{
		x: (theSecondaryPolygonFill[1][0] / 2) + ((theLeftPolygon[1][0] + gap) / 2) - 6,
		y: (theSecondaryPolygonFill[3][1] / 2)
	});


	// ------------------------------------------------------------------------- //

	var layers = dom.getTimeline().layers;

	for (var i = 0; i < layers.length; i++)
	{
		if (layers[i].name.toUpperCase().indexOf('MASK') > -1)
		{
			dom.getTimeline().setSelectedLayers(i, i);
			dom.getTimeline().setLayerProperty('layerType', 'mask');
		}
		else if (layers[i].name.toUpperCase().indexOf('FILL') > -1)
		{
			dom.getTimeline().setSelectedLayers(i, i);
			dom.getTimeline().setLayerProperty('layerType', 'masked');
		}
	}

	dom.exitEditMode();
}

// ------------------------------------------------------------------------- //

function drawBackground()
{
	lib.addNewItem('movie clip', titleBackground);

	lib.editItem(titleBackground);
	dom.setCustomFill(myBackroundFill);
	dom.addNewRectangle(
	{
		left: 0,
		top: 0,
		right: stageW,
		bottom: stageH
	}, 0);

	fl.getDocumentDOM().exitEditMode();
}

// ------------------------------------------------------------------------- //

function drawKeyline()
{
	lib.addNewItem('movie clip', titleKeyline);

	lib.editItem(titleKeyline);

	//fill
	var myFill = dom.getCustomFill();
	myFill.style = "solid";
	myFill.color = 0x777777;
	dom.setCustomFill(myFill);

	// stroke
	// Fix from answer: http://stackoverflow.com/questions/4869979/jsfl-setcustomstroke-does-not-work-with-stroke-style-nostroke
	dom.swapStrokeAndFill();
	var tempFill = dom.getCustomFill("toolbar");
	tempFill.style = "noFill";
	dom.setCustomFill(tempFill);
	dom.swapStrokeAndFill();

	// draw rectangle

	dom.addNewRectangle(
	{
		left: 0,
		top: 0,
		right: stageW,
		bottom: 1
	}, 0);
	dom.addNewRectangle(
	{
		left: stageW - 1,
		top: 0,
		right: stageW,
		bottom: stageH
	}, 0);
	dom.addNewRectangle(
	{
		left: 0,
		top: stageH - 1,
		right: stageW,
		bottom: stageH
	}, 0);
	dom.addNewRectangle(
	{
		left: 0,
		top: 0,
		right: 1,
		bottom: stageH
	}, 0);

	fl.drawingLayer.endDraw();

	fl.getDocumentDOM().exitEditMode();
}

// ------------------------------------------------------------------------- //

function addKeylines()
{
	// JSFL
	var currentTimeline = fl.getDocumentDOM().getTimeline();

	var stageH = fl.getDocumentDOM().height;
	var stageW = fl.getDocumentDOM().width;

	var currentGuides = new XML(currentTimeline.getGuidelines());

	// clear guides
	currentTimeline.setGuidelines(new XML('<guidelines></guidelines>'));

	var additionalGuides = [
		'<guideline direction="v">' + 0 + '</guideline>',
		'<guideline direction="v">' + bezelHorizontal + '</guideline>',
		'<guideline direction="h">' + 0 + '</guideline>',
		'<guideline direction="h">' + bezelVertical + '</guideline>',
		'<guideline direction="v">' + (stageW - bezelHorizontal) + '</guideline>',
		'<guideline direction="v">' + stageW + '</guideline>',
		'<guideline direction="h">' + (stageH - bezelHorizontal) + '</guideline>',
		'<guideline direction="h">' + stageH + '</guideline>'
	];

	for (var i = 0; i < additionalGuides.length; i++)
	{
		currentGuides.appendChild(new XML(additionalGuides[i]));
	}

	currentTimeline.setGuidelines(new XML(currentGuides));
}

// ------------------------------------------------------------------------- //

function createAndAddBtnBackground()
{
	var lib = fl.getDocumentDOM().library;

	lib.addNewItem('button', titleBtnBackground);

	if (lib.getItemProperty('linkageImportForRS') == true)
	{
		lib.setItemProperty('linkageImportForRS', false);
	}
	else
	{
		lib.setItemProperty('linkageExportForAS', false);
		lib.setItemProperty('linkageExportForRS', false);
	}

	var btnBackground = lib.items[lib.findItemIndex(titleBtnBackground)];

	dom.getTimeline().addNewLayer(titleBtnBackground);

	dom.addItem(
	{
		x: 0,
		y: 0
	}, btnBackground);


	fl.getDocumentDOM().getTimeline().layers[0].frames[0].elements[0].actionScript = "on (release) {\n	EB.Clickthrough('Primary Clickthrough');\n}";

	fl.getDocumentDOM().enterEditMode('inPlace');

	// rtyrtyrty

	var background = lib.items[lib.findItemIndex(titleBackground)];
	fl.getDocumentDOM().addItem(
	{
		x: 0,
		y: 0,
	}, background);

	fl.getDocumentDOM().align('left', true);
	fl.getDocumentDOM().align('top', true);

	// add in extra frames for button
	fl.getDocumentDOM().getTimeline().insertFrames();
	fl.getDocumentDOM().getTimeline().insertFrames();
	fl.getDocumentDOM().getTimeline().insertFrames();

	fl.getDocumentDOM().exitEditMode();
}

// ------------------------------------------------------------------------- //

function addHeaderAndKeylineToStage()
{

	var lib = fl.getDocumentDOM().library;

	// ------------------------------------------------------------------------------ //

	var header = lib.items[lib.findItemIndex(titleHeader)];

	dom.getTimeline().addNewLayer(titleHeader);

	fl.getDocumentDOM().addItem(
	{
		x: 0,
		y: 0,
	}, header);
	fl.getDocumentDOM().align('left', true);
	fl.getDocumentDOM().align('top', true);

	// ------------------------------------------------------------------------------//

	var keyline = lib.items[lib.findItemIndex(titleKeyline)];

	dom.getTimeline().addNewLayer(titleKeyline);

	fl.getDocumentDOM().addItem(
	{
		x: 0,
		y: 0,
	}, keyline);
	fl.getDocumentDOM().align('left', true);
	fl.getDocumentDOM().align('top', true);

	// ------------------------------------------------------------------------------//
}

// ------------------------------------------------------------------------- //

if (lib.findItemIndex(titleHeader) == '')
{
	var result = fl.getDocumentDOM().xmlPanel(fl.configURI + "Commands/[Dog] DrawHeader.xml");

	if (result.dismiss == "accept")
	{
		drawBackground();
		createAndAddBtnBackground();

		drawHeader();
		drawKeyline();
		addHeaderAndKeylineToStage();
		addKeylines();
	}
}
else
{
	alert('Header clip already exists. Please delete and try again.');
}