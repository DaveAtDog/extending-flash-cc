// utility
function tr(){fl.trace(Array.prototype.slice.call(arguments));}


var dom = fl.getDocumentDOM();
var timeline  = dom.getTimeline();

var result = dom.xmlPanel(fl.configURI + "Commands/[Dog] AddGuidline.xml");

if (result.dismiss == "accept")
{
	var currentGuides = new XML(timeline.getGuidelines());
	
	// Reset guidelines, we'll add all original guidelines back later
	timeline.setGuidelines(new XML(<guidelines></guidelines>));
	
	var orientation = (result.orientation == 'y') ? 'h' : 'v';
	
	var rawResult = result.position;
	
	var resultArray = rawResult.split(',');
	
	for(var i = 0; i < resultArray.length; i++)
	{
		var newGuide = '<guideline direction="' + orientation + '">' + eval(resultArray[i]) + '</guideline>';
	
		currentGuides.appendChild(new XML(newGuide));
	}
	
	timeline.setGuidelines(new XML(currentGuides));
	
	//tr(result,result.length);
}
