// utility
function tr(){fl.trace(Array.prototype.slice.call(arguments));}


var dom = fl.getDocumentDOM();
dom.getTimeline().setSelectedFrames([]);

var frameRate = dom.frameRate;

var currentFrame = dom.getTimeline().currentFrame;

var framesToMove = Math.round(frameRate/2);

dom.getTimeline().currentFrame = currentFrame - framesToMove;
