/**
 * Renames layers according first item in layer. With coding standards warnings. Folders are skipped by default.
 * @version 1.5
 * @author: Mediamonks - http://www.mediamonks.com
 * @author: Mark Knol  - http://blog.stroep.nl
 */

fl.outputPanel.clear();
var doc = fl.getDocumentDOM();
autoNameLayers(true);

function autoNameLayers(showWarnings)
{
	var timeline = doc.getTimeline();

	for (var i = timeline.layers.length - 1; i >= 0; i--)
	{
		var layer = timeline.layers[i];

		if (layer.layerType != "guide" && layer.layerType != "mask" && layer.layerType != "folder")
		{
			var frameNumber = 0;
			var element;

			var layerHasCode = false;
			var layerHasLabel = false;
			var layerHasComment = false;
			var layerHasAnchor = false;
			var layerHasSound = false;

			for (var j = 0, totalFrames = layer.frames.length; j < totalFrames; j++)
			{
				var frame = layer.frames[j];

				if (j === frame.startFrame) // keyframes only
				{
					element = frame.elements[0]; // first element on frame1

					if (frame.soundLibraryItem) layerHasSound = true;
					if (frame.labelType != "none")
					{
						if (frame.labelType == "name") layerHasLabel = true;
						if (frame.labelType == "comment") layerHasComment = true;
						if (frame.labelType == "anchor") layerHasAnchor = true;
					}
					// test for code
					if (frame.actionScript && frame.actionScript.length > 0)
					{
						layerHasCode = true;
					}
				}
			}

			if (!element && !layerHasCode && !layerHasLabel && !layerHasComment && !layerHasAnchor && !layerHasSound)
			{
				// remove empty layers
				timeline.deleteLayer(i);
			}
		}
	}
}