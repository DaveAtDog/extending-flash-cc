/**
 * Lock's all mask layers.
 */

// utility
function tr()
{
	fl.trace(Array.prototype.slice.call(arguments));
}

function setState(state)
{
	var doc = fl.getDocumentDOM();
	var tl = doc.getTimeline();
	var layers = tl.layers;

	for (var i = 0; i < layers.length; i++)
	{
		if (layers[i].layerType == "mask")
		{
			tl.setSelectedLayers(i, i);
			tl.setLayerProperty("locked", !state, "selected");
			tl.setLayerProperty("visible", state, "selected");
			tl.setLayerProperty("outline", state, "selected");
		}
	}
}